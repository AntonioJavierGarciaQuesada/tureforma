$(document).ready(function(){
    //Iniciar barra de navegacion
    $('.sidenav').sidenav();

    //Inciar datepicker
    $('.datepicker').datepicker();

    //Iniciar Carrusel
    $('.carousel').carousel({
      duration:100,
      fullWidth: true,  
      indicators:true
    });
    
    //Envento Scroll para cambiar el formato del navbar
    $(document).scroll(function () {
      var $nav = $(".nav-extended");
      $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });

    //Iniciar efecto parallax
    $('.parallax').parallax();
   
    //Iniciar Ventana Modal
    $('.modal').modal({
        opacity:0.3,
        inDuration:800,
        outDuration:200,
      
    });
    
    //Iniciar select     
   $('select').formSelect();
    
   //Iniciar tabs
   //$('#tabs-quiero').tabs();
 });