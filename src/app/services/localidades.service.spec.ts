import { TestBed, inject } from '@angular/core/testing';

import { LocalidadesService } from './localidades.service';

describe('LocalidadesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocalidadesService]
    });
  });

  it('should be created', inject([LocalidadesService], (service: LocalidadesService) => {
    expect(service).toBeTruthy();
  }));
});
