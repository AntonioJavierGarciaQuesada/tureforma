import { Proyecto } from './../models/proyecto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GLOBAL } from './global'

@Injectable({
  providedIn: 'root'
})
export class ProyectosService {
  private url:String;
  
  constructor(private _http:HttpClient) { 
    this.url = GLOBAL.url;
  }

  saveProyecto(proyecto:Proyecto,token):Observable<any>{
    let proyectoJson = JSON.stringify(proyecto);
    console.log(proyectoJson);
    let headers = new HttpHeaders().set('Content-Type','application/json')
                                   .set('Authorization',token);
    return this._http.post(this.url+'proyecto/crear', proyectoJson, {headers:headers});
  }

  getProyectos():Observable<any>{
    return this._http.get(this.url+'proyecto/listado_general');
  }

  getMisProyectos(token):Observable<any>{
    let headers = new HttpHeaders().set('Authorization',token); 
    return this._http.get(this.url+'proyecto/mis_proyectos',{headers:headers});
  }


}
