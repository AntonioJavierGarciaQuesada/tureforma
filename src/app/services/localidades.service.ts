import { Localidad } from './../models/localidad';
import { localidades } from './../mocks/localidades.mocks';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalidadesService {
  localidadesFiltro: Localidad[] = localidades;
  constructor() { }
  
  ngOnInit() {
   
  }
  getProvincia():string[]{
    let provincias: string[];
    provincias =this.localidadesFiltro.map(localidad=>localidad.provincia);
    return provincias.filter(this.unique);
  }
  getComunidades():string[]{
    let comunidades : string[];
    comunidades = this.localidadesFiltro.map(localidad=>localidad.comunidad);
    return comunidades.filter(this.unique); 
  }

  getProvincias(comunidad:string):string[]{
    let provincias: string[];
    provincias = this.localidadesFiltro.map(localidad=>{
      if(localidad.comunidad === comunidad) return localidad.provincia;
    });
    return provincias.filter(this.unique);     
  }

  getLocalidades(provincia:string){
    let localidades: string[];
    localidades = this.localidadesFiltro.map(localidad=>{
      if(localidad.provincia === provincia) return localidad.localidad;
    });
    return localidades.filter(this.unique); 
  }

  private unique(value, index, self) { 
    return self.indexOf(value) === index;
  }
}
