//Modulos
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
//Modelos
import { Mensaje } from './../models/mensaje';
//Ruta
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class MensajeService {
  public url: String;
  
  constructor(private _http:HttpClient) {
    this.url = GLOBAL.url;
  }

  enviarMensaje(token, mensaje):Observable<any> {
    let mensajeJson = JSON.stringify(mensaje);
    let headers = new HttpHeaders().set('Content-Type','application/json')
                                   .set('Authorization', token);
    return this._http.post(this.url+'mensaje/enviar',mensajeJson,{headers:headers});
  }

  getMensajesRecibidos(token,page = 1):Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/json')
                                   .set('Authorization', token);
    return this._http.get(this.url+'mensaje/mis-mensajes-recibidos/'+page,{headers:headers});
  }

  getMensajesEnviados(token,page = 1):Observable<any>{
    let headers = new HttpHeaders().set('Content-Type','application/json')
                                   .set('Authorization', token);  
    return this._http.get(this.url+'mensaje/mis-mensajes-enviados/'+page,{headers:headers});
    
  }





}
