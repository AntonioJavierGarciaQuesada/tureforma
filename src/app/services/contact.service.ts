import { MensajeContacto } from './../models/mensajeContacto';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import {GLOBAL} from './global';
@Injectable({
  providedIn: 'root'
})
export class ContactService{
  private url:String;
  constructor(private _http: HttpClient) {
    this.url = GLOBAL.url;
   }

  enviarMensajeContacto(mensaje:MensajeContacto):Observable<any>{
    let params = JSON.stringify(mensaje);
    let headers = new HttpHeaders().set('Content-Type','application/json');

    return this._http.post(this.url+'contacto/', params , {headers:headers} );
  }
}
