import { TestBed, inject } from '@angular/core/testing';

import { UsurioService } from './usurio.service';

describe('UsurioService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsurioService]
    });
  });

  it('should be created', inject([UsurioService], (service: UsurioService) => {
    expect(service).toBeTruthy();
  }));
});
