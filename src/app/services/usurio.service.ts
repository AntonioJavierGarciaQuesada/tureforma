import { Usuario } from './../models/usuario';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class UsurioService {
  public url: String;
  public usuarioRegistrado;
  public token;
  
  constructor(public _http: HttpClient) {
    this.url = GLOBAL.url;
   }

  registro(usuario:Usuario):Observable<any>{
    let usuarioJson = JSON.stringify(usuario);
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'signin',usuarioJson,{headers:headers});
  }

  login (loginData):Observable<any>{
    let loginJson = JSON.stringify(loginData);
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'login',loginJson,{headers:headers});
  }
  
  logoOut(){
      localStorage.removeItem('token');
      localStorage.removeItem('usuarioData');
      this.usuarioRegistrado = undefined;
    
  }

  getRol(){
    let usuarioRegistrado = JSON.parse(localStorage.getItem('usuarioData'));
    if(usuarioRegistrado != undefined ) this.usuarioRegistrado= usuarioRegistrado;
    else this.usuarioRegistrado = null;    
    return this.usuarioRegistrado.tipo;
  }

  getUsuario(){
    let usuarioRegistrado = JSON.parse(localStorage.getItem('usuarioData'));
    if(usuarioRegistrado != undefined ) this.usuarioRegistrado= usuarioRegistrado;
    else this.usuarioRegistrado = null;    
    return this.usuarioRegistrado;
  }

  getUsuarioById(id):Observable<any>{
    let headers = new HttpHeaders().set('Authorization', this.getToken());
    return this._http.get(this.url+'user/'+id,{headers:headers});
  }
  getToken(){
    let token = JSON.parse(localStorage.getItem('token'));
    if(token != 'undefined') this.token = token;
    else this.token = null;
    return this.token;
  }

  recuperar(emailR):Observable<any>{
    let emailJson = JSON.stringify({email:emailR});
   
    let headers = new HttpHeaders().set('Content-Type','application/json');
    return this._http.post(this.url+'recuperar-password',emailJson,{headers:headers});
  }

  cambiarPassword(pass):Observable<any>{
   let passJson = JSON.stringify(pass);
 
   let headers = new HttpHeaders().set('Content-Type','application/json')
                                  .set('Authorization', this.getToken());
    return this._http.put(this.url+'cambiar-password',passJson,{headers:headers});
             
  }

  updateUser(usuario:Usuario):Observable<any>{    
    let params = JSON.stringify(usuario);
    let headers = new HttpHeaders().set('Content-Type','application/json')
    .set('Authorization', this.getToken());

    return this._http.put(this.url+'update-user/'+usuario._id, params,{headers:headers});
  }
  getAutonomos():Observable<any>{
    return this._http.get(this.url+'autonomos');
  }
  getClientes():Observable<any>{
    let headers = new HttpHeaders().set('Authorization', this.getToken());
    return this._http.get(this.url+'clientes',{headers:headers});
  }
}
