import { Localidad } from './localidad';
export class Proyecto{
    constructor(
        public cliente:Number,
        public categoria:String,
        public descripcion_breve:String,
        public descripcion_completa:String,
        public precio:Number,
        public duracion:String,
        public size:String,
        public direccion:String,
        public localidad:String,
        public provincia:String,
        public comunidad:String,
        public incluir_material:Boolean,        
        public createdAt?:Date,
        public path_photos?:String

    ){}
}