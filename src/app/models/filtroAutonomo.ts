export class FiltroAutonomo{
    constructor(
        public categoria: String,
        public provincia: String,
        public precio_max: Number,
        public precio_min: Number 
    ){}
}