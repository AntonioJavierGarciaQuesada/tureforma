import { Habilidad } from './habilidad';
import { Perfil } from "./perfil";

export class Usuario{
    constructor(      
        public email:String,
        public nombreUsuario:String,
        public password:String,
        public tipo:String,
        public createdAt?:String,
        public _id?:Number,
        public avatar?:String,
        public perfil?:Perfil,
        public experiencia?:Habilidad
    ){}
}