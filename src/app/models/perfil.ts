export class Perfil{
    constructor(
        public nombre: String,
        public apellidos: String,        
        public movil: Number,        
        public fecha_nacimiento: Date,        
        public direccion: String,
        public localidad: String,
        public provincia: String,
        public comunidad: String,
        public telefono?: Number,      
        public dni?: String,
        public cif?:string,
        public habilidades?:String              
    ){}
}