export class MensajeContacto{
    constructor(
        public nombre:String,
        public apellidos: String,        
        public emailC: String,
        public texto:String,
        public telefono?: String
    ){}
}