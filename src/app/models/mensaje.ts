export class Mensaje{
    constructor(
        public _id:Number,
        public emisor:Number,
        public receptor:Number,
        public texto:String,
        public createdAt: Date,
        public viwed: Boolean
    ) {}

}