export class Habilidad{
    constructor(
        public tipo_habilidad:String,
        public year: Number,
        public descripcion: String,
        public tipo_trabajo:String,
        public salario_hora:Number
    ){}
}