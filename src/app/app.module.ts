
//Componentes
import { HeaderComponent } from './componentes/header/header.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { NavbarComponent } from './componentes/navbar/navbar.component';
import { MainComponent } from './componentes/sections/main/main.component';
import { LoginComponent } from './componentes/navbar/login/login.component';
import { RegistroComponent } from './componentes/navbar/registro/registro.component';
import { ContactemeComponent } from './componentes/sections/contacteme/contacteme.component';
import { ComoFuncionaComponent } from './componentes/sections/como-funciona/como-funciona.component';
import { TrabajadorComponent } from './componentes/sections/como-funciona/trabajador/trabajador.component';
import { ClienteComponent } from './componentes/sections/como-funciona/cliente/cliente.component';

//Modulos Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'

//Modulo Custom Mensajes Privados.
import { MensajesModule } from './module/mensajes/mensajes.module';

//Import Library Date
import { MomentModule } from 'angular2-moment';

//Material Angular
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './module/material.module';
import 'hammerjs';

//Rutas
import { HashLocationStrategy,LocationStrategy} from '@angular/common'
import { routes } from './routes/router-main';

//Componetes
import { RecuperarComponent } from './componentes/navbar/recuperar/recuperar.component';
import { PerfilComponent } from './componentes/sections/area-usuario/perfil/perfil.component';
import { DUsuarioComponent } from './componentes/sections/area-usuario/perfil/d-usuario/d-usuario.component';
import { DIdentifyComponent } from './componentes/sections/area-usuario/perfil/d-identify/d-identify.component';
import { DExperienciaComponent } from './componentes/sections/area-usuario/perfil/d-experiencia/d-experiencia.component';
import { FormPerfilComponent } from './componentes/sections/area-usuario/perfil/form-perfil/form-perfil.component';
import { AutonomosComponent } from './componentes/sections/autonomos/autonomos.component';
import { FormExperienciaComponent } from './componentes/sections/area-usuario/perfil/form-experiencia/form-experiencia.component';
import { ProyectosComponent } from './componentes/sections/proyectos/proyectos.component';
import { TusProyectosComponent } from './componentes/sections/area-usuario/tus-proyectos/tus-proyectos.component';
import { FormProyectosComponent } from './componentes/sections/area-usuario/tus-proyectos/form-proyectos/form-proyectos.component';
import { FiltroComponent } from './componentes/sections/autonomos/filtro/filtro.component';
import { AutonomosPipe } from './componentes/sections/autonomos/pipe/autonomos.pipe';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    MainComponent,
    LoginComponent,
    RegistroComponent,
    ContactemeComponent,
    ComoFuncionaComponent,
    TrabajadorComponent,
    ClienteComponent,
    RecuperarComponent,
    PerfilComponent,
    DUsuarioComponent,
    DIdentifyComponent,   
    FormPerfilComponent,
    AutonomosComponent,
    DExperienciaComponent,
    FormExperienciaComponent,
    ProyectosComponent,
    TusProyectosComponent,
    FormProyectosComponent,
    FiltroComponent,
    AutonomosPipe 
      
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MomentModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    routes,
    MensajesModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
