import { UsurioService } from './../../../services/usurio.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../models/usuario';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  signinForm:FormGroup;
  usuario: Usuario;
  status:string;
  constructor(private _usuarioServicio:UsurioService) {
   
   }

  ngOnInit() {
    this.signinForm = new FormGroup({
      nombre_usuario: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^[a-zñ]{1}[a-zñáéíóú1-9_-]+$/),
        Validators.minLength(5),
        
      ]),
      email: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
        Validators.minLength(8)
      ]),
     
      tipo: new FormControl(null, [
        Validators.required
      ]),
      password: new FormControl(null, [
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ]),
      password_confirm: new FormControl(null, [
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ])
    })
  }

  submit(){
    if( this.signinForm.valid ){
      const {nombre_usuario,email,password,tipo} = this.signinForm.value;
      const usuario = new Usuario(email,nombre_usuario,password,tipo);

      this._usuarioServicio.registro(usuario).subscribe(
        response => {         
            this.signinForm.reset();
            this.status= "success";
            localStorage.setItem('usuarioData',JSON.stringify(response.usuarioStored));
            localStorage.setItem('token',JSON.stringify(response.token));                 
        },
        err => {
          this.status = "error";
        }
      );
    }
  }
  ngDoCheck(){
    this.usuario = this._usuarioServicio.getUsuario();
  }
  

}
