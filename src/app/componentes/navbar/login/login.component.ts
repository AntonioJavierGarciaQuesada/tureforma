import { Router } from '@angular/router';
import { UsurioService } from './../../../services/usurio.service';
import { Usuario } from './../../../models/usuario';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
   usuario:Usuario;
  status:String;
  constructor(private _usuarioService:UsurioService,private router:Router) {
     console.log(this.usuario);
      if(this.usuario && this.usuario.email){      
        this.router.navigate['/perfil'];
      } 
   }

  ngOnInit() {
    this.usuario = this._usuarioService.getUsuario();
    this.loginForm = new FormGroup({
      emailL: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
        Validators.minLength(8)
      ]),
      passwordL: new FormControl(null, [
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ])
    })
  }

  submit(){

    if( this.loginForm.valid ){
      let loginData={
          email:this.loginForm.value.emailL,
          password:this.loginForm.value.passwordL
      };
     this._usuarioService.login(loginData)
      .subscribe(
        response => {
            this.loginForm.reset();
            if(response.usuario && response.usuario._id) {
              this.status= "success";
              localStorage.setItem('usuarioData',JSON.stringify(response.usuario));
              localStorage.setItem('token',JSON.stringify(response.token));             
            }else{
              this.status = "error"; 
                     
            }    
        },
        err => {
          this.status = "error";
        }
      )
    }
   
  }
  ngDoCheck(){
    this.usuario = this._usuarioService.getUsuario();
    if(this.usuario == undefined && this.status != "error") this.status = null ;
  }
  
  ngOnDestroy() {
    
  }

}
