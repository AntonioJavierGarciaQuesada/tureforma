import { UsurioService } from './../../../services/usurio.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.component.html',
  styleUrls: ['./recuperar.component.css']
})
export class RecuperarComponent implements OnInit {
  recuperarForm: FormGroup;
  mensaje:String;
  constructor(private _usuarioService: UsurioService) { }

  ngOnInit() {
    this.recuperarForm = new FormGroup({
      emailR: new FormControl(null,[
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
        Validators.minLength(8)
      ])
    });
  }

  submit(){
    if( this.recuperarForm.valid ){
      let email = this.recuperarForm.value.emailR;
      this._usuarioService.recuperar(email)
        .subscribe(
          response =>{
            console.log(response);
            this.mensaje = response.mensaje;
            
          },
          err => {
           
            this.mensaje = err.mensaje;
          });
          this.recuperarForm.reset();
    }
  }

}
