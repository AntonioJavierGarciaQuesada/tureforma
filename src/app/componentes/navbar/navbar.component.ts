import { UsurioService } from './../../services/usurio.service';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../models/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  usuarioRegistrado: Usuario;
  constructor(private _usuarioService:UsurioService,private router:Router) { }

  ngOnInit() {
    this.usuarioRegistrado = this._usuarioService.getUsuario();
  }
  logout(){
    this._usuarioService.logoOut();
    this.usuarioRegistrado = null;
    this.router.navigate(['/']);
  }
  ngDoCheck(){
    this.usuarioRegistrado = this._usuarioService.getUsuario(); 
  }
}
