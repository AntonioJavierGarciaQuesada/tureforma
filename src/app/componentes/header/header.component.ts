import { Component, OnInit } from '@angular/core';
import { UsurioService } from '../../services/usurio.service';
import { Usuario } from '../../models/usuario';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  usuarioRegistrado:Usuario;
  constructor(private _usuarioService:UsurioService) { }

  ngOnInit() { 
   
    this.usuarioRegistrado = this._usuarioService.getUsuario();

  }
  isCliente(){
    return this.usuarioRegistrado && this.usuarioRegistrado.tipo === "Cliente";
  }
  isRegistrado(){
    return this.usuarioRegistrado;
  }
  ngDoCheck() {
    this.usuarioRegistrado = this._usuarioService.getUsuario();
  }
}
