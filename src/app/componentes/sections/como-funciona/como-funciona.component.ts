import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-como-funciona',
  templateUrl: './como-funciona.component.html',
  styleUrls: ['./como-funciona.component.css']
})
export class ComoFuncionaComponent implements OnInit {
  tabActive:string = "trabajar";
  constructor() { }

  ngOnInit() {
  }
  
  cambiarTab(tabName){
    this.tabActive = tabName;
  }

}
