import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTableDataSource,MatPaginator,MatSort } from '@angular/material';
import { ProyectosService } from '../../../services/proyectos.service';
import { UsurioService } from '../../../services/usurio.service';
import { Usuario } from '../../../models/usuario';


@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.css']
})
export class ProyectosComponent implements OnInit {
  
  proyectosList = [];
  proyectoSelect;
  displayedColumns = ['id'];
  dataSource;
  visible: Boolean ; 
  usuarioRegistrado:Usuario;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor(
    private _proyectoService:ProyectosService,
    private _usuarioService:UsurioService
  ) { }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //quitamos los espacios al filtro
    filterValue = filterValue.toLowerCase(); //cambiamos a minuscula lo introducido en el filtro
    this.dataSource.filter = filterValue;
  }


  ngOnInit() {
    
   
    this._proyectoService.getProyectos()
      .subscribe(
        response=>{
          console.log(response);
          if(response.proyectos && response.proyectos.legth !==0){
            this.proyectosList = response.proyectos;
            this.dataSource = new MatTableDataSource(this.proyectosList);
            this.dataSource.paginator = this.paginator;
            this.visible = false;
             
          }         
         
        },
        err =>{
          console.log(err);
        }
        
      ) 
      this.usuarioRegistrado = this._usuarioService.getUsuario();
  }

  isAutonomo(){
    return this.usuarioRegistrado &&  this.usuarioRegistrado.tipo && this.usuarioRegistrado.tipo === 'Autonomo';
  }
  

 

}
