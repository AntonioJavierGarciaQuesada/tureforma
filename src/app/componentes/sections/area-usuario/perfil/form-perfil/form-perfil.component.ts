import { Perfil } from './../../../../../models/perfil';
import { Localidad } from './../../../../../models/localidad';
import { UsurioService } from './../../../../../services/usurio.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LocalidadesService } from '../../../../../services/localidades.service';
import { Usuario } from '../../../../../models/usuario';
import { Moment } from 'moment';

@Component({
  selector: 'app-form-perfil',
  templateUrl: './form-perfil.component.html',
  styleUrls: ['./form-perfil.component.css']
})
export class FormPerfilComponent implements OnInit {
  perfilForm: FormGroup;
  tipo:String;
  usuarioRegistrado: Usuario;
  status:String;
  public comunidades:string[];
  public provincias:string[];
  public localidades:string[];
  public min:Date;
  
  constructor(private _usuarioService: UsurioService,private localidadesService:LocalidadesService) { }

  ngOnInit() {
    this.usuarioRegistrado = this._usuarioService.getUsuario();
    this.tipo = this.usuarioRegistrado.tipo;
    this.comunidades = this.localidadesService.getComunidades();
    
    this.perfilForm = new FormGroup({
      nombre: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/),
        Validators.min(3)
      ]),
      apellidos: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/),
        Validators.min(3)
      ]),
      dni: new FormControl(null,[
        Validators.pattern(/^[0-9]{8}[A-Z]{1}$/)
      ]),
      cif: new FormControl(null,[
        Validators.pattern(/^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/)
      ]),
      fecha_nacimiento: new FormControl(null,Validators.required),
      direccion: new FormControl(null,[
        Validators.required
      ]),
      telefono:new FormControl(null,[
        Validators.pattern(/^([6-9]{1}[0-9]{8})+$/),
        Validators.min(9)
      ]),
      movil:new FormControl(null,[
        Validators.required,
        Validators.pattern(/^([6,8]{1}[0-9]{8})+$/),
        Validators.min(9)
      ]),
      comunidad:new FormControl(null, Validators.required),
      provincia: new FormControl(null, Validators.required),
      localidad: new FormControl(null, Validators.required)
    });
   if(this.usuarioRegistrado.perfil){    
    
    this.perfilForm.controls.nombre.setValue(this.usuarioRegistrado.perfil.nombre);
    this.perfilForm.controls.apellidos.setValue(this.usuarioRegistrado.perfil.apellidos);
    this.perfilForm.controls.dni.setValue(this.usuarioRegistrado.perfil.dni);
    this.perfilForm.controls.cif.setValue(this.usuarioRegistrado.perfil.cif);
    this.perfilForm.controls.fecha_nacimiento.setValue(this.usuarioRegistrado.perfil.fecha_nacimiento);
    this.perfilForm.controls.telefono.setValue(this.usuarioRegistrado.perfil.telefono);
    this.perfilForm.controls.movil.setValue(this.usuarioRegistrado.perfil.movil);
    this.perfilForm.controls.direccion.setValue(this.usuarioRegistrado.perfil.direccion);
    this.perfilForm.controls.localidad.setValue(this.usuarioRegistrado.perfil.localidad);
    this.perfilForm.controls.comunidad.setValue(this.usuarioRegistrado.perfil.comunidad);
    this.perfilForm.controls.provincia.setValue(this.usuarioRegistrado.perfil.provincia);
  
   }
  }


  isCliente(){
    return this.tipo === 'Cliente';
  }

  isAutonomo(){
    return this.tipo === 'Autonomo';
  }

  selectComunidad(){
    this.provincias  = this.localidadesService.getProvincias(this.perfilForm.value.comunidad);
  }

  selectProvincia(){    
    this.localidades = this.localidadesService.getLocalidades(this.perfilForm.value.provincia);   

  }

  submitFormPerfil(){
    if(this.perfilForm.valid){
      this.usuarioRegistrado.perfil = this.perfilForm.value; 
    if(this.usuarioRegistrado.tipo === 'Cliente'){
      this.usuarioRegistrado.perfil.habilidades=undefined;
      this.usuarioRegistrado.perfil.cif=undefined;
    }else{
      this.usuarioRegistrado.perfil.dni=undefined;
    }      
    
      this._usuarioService.updateUser(this.usuarioRegistrado).subscribe(
          response=>{
            if(!response.user){
              this.status = 'error';
            }else{
              this.status = 'success';
              localStorage.setItem('usuarioData',JSON.stringify(this.usuarioRegistrado));
              this.perfilForm.reset();
            }
          },
          error=>{
            let errorMensage = <any>error;
            console.log(errorMensage);
            if(errorMensage != null) this.status= 'error';

          }
      );
    
    }
  }

}
