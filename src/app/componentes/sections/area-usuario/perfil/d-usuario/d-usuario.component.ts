import { GLOBAL } from './../../../../../services/global';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UsurioService } from './../../../../../services/usurio.service';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../../models/usuario';
import { UploadService } from '../../../../../services/upload.service';

@Component({
  selector: 'app-dUsuario',
  templateUrl: './d-usuario.component.html',
  styleUrls: ['./d-usuario.component.css']
})
export class DUsuarioComponent implements OnInit {
  usuarioRegistrado:Usuario;
  cambiarPassForm:FormGroup; 
  cambiarFotoForm:FormGroup; 
  filesToUpload: Array<File>;
  formCP:String; 
  formCF:String;  
  status:String;
  statusF:String;
  url:String;

  constructor(
      private _usuarioService:UsurioService,
      private _uploadService:UploadService) { 
        this.url = GLOBAL.url;
      }

  ngOnInit() {
    this.cambiarPassForm = new FormGroup({
      passwordA: new FormControl(null,[
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ]),
      password_confirmA: new FormControl(null, [
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ]),
      passwordN: new FormControl(null,[
        Validators.required, 
        Validators.minLength(8),
        Validators.maxLength(15)
      ]),
    });
    this.cambiarFotoForm = new FormGroup({
      filePerfil: new FormControl(null,Validators.required)
    });    
  }

  submit(){
    
    if( this.cambiarPassForm.valid ){      
      const pass={
        pass_old:this.cambiarPassForm.value.passwordA,
        pass_new:this.cambiarPassForm.value.passwordN
      }
      this._usuarioService.cambiarPassword(pass)
        .subscribe(
          response=>{
           
            if(response && response.status == 'success'){              
              this.status = 'success';
              this.formCP = undefined;

              this.cambiarPassForm.reset();
            }else{
              this.status = 'error';
            }
          },  
            err =>{
              console.log(err);
            }
        )     
    }
  }
  submitFoto(){
    if( this.cambiarFotoForm.valid ){  
      this._uploadService
      .makeFileRequest(this.url+'upload-image-user/'+this.usuarioRegistrado._id,[],this.filesToUpload, this._usuarioService.getToken(), 'image')
      .then(
        (result:any)=>{ 
          if(result && result.user && result.user.avatar){
            this.usuarioRegistrado.avatar = result.user.avatar;          
            localStorage.setItem('usuarioData',JSON.stringify(this.usuarioRegistrado));
            this.formCF = undefined;
            this.cambiarFotoForm.reset();
            this.statusF = "success";
          }else{
            this.statusF = "error";
          }     
         
        }
      );
    }
  }

  fileChangeEvent(fileInput: any){
   
    this.filesToUpload = <Array<File>>fileInput.target.files;
    
  }


  abrirFormCP(){
    this.formCP = 'active';
    this.status = undefined;
  }
  abrirFormCF(){
    this.formCF = 'active';
    this.status = undefined;
  }

  ngDoCheck(){
    this.usuarioRegistrado= this._usuarioService.getUsuario();
  }

}
