import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DExperienciaComponent } from './d-experiencia.component';

describe('DExperienciaComponent', () => {
  let component: DExperienciaComponent;
  let fixture: ComponentFixture<DExperienciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DExperienciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DExperienciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
