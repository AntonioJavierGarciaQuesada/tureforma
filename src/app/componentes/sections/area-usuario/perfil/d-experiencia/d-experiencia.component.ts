import { Router } from '@angular/router';
import { UsurioService } from './../../../../../services/usurio.service';

import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../../models/usuario';

@Component({
  selector: 'app-dExperiencia',
  templateUrl: './d-experiencia.component.html',
  styleUrls: ['./d-experiencia.component.css']
})
export class DExperienciaComponent implements OnInit {
  usuarioRegistrado:Usuario;

  constructor(private _usuarioService:UsurioService,private _router:Router) { }

  ngOnInit() {
    this.usuarioRegistrado= this._usuarioService.getUsuario();
    if(!this.usuarioRegistrado.experiencia || !this.usuarioRegistrado.experiencia.tipo_habilidad)
      this._router.navigate(['/perfil/update-experiencia'])
  }
  ngDoCheck(){
    this.usuarioRegistrado= this._usuarioService.getUsuario();
  }

  


}
