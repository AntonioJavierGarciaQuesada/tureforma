import { UsurioService } from './../../../../../services/usurio.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Usuario } from '../../../../../models/usuario';


@Component({
  selector: 'app-form-experiencia',
  templateUrl: './form-experiencia.component.html',
  styleUrls: ['./form-experiencia.component.css']
})
export class FormExperienciaComponent implements OnInit {
  experienciaForm: FormGroup;
  usuarioRegistrado: Usuario;
  status:String;
  constructor(private _usuarioService:UsurioService) { }

  ngOnInit() {
    this.usuarioRegistrado = this._usuarioService.getUsuario();
    this.experienciaForm = new FormGroup({
      tipo_habilidad: new FormControl(null,Validators.required),
      year: new FormControl(null, Validators.required),
      descripcion: new FormControl(null,[
        Validators.required,
        Validators.maxLength(300)
      ]),
      tipo_trabajo: new FormControl(null, Validators.required ),
      salario_hora: new FormControl(null, Validators.required) 
    });
    if(this.usuarioRegistrado.experiencia){
    
      this.experienciaForm.controls.tipo_habilidad.setValue(this.usuarioRegistrado.experiencia.tipo_habilidad);
      this.experienciaForm.controls.year.setValue(this.usuarioRegistrado.experiencia.year);
      this.experienciaForm.controls.descripcion.setValue(this.usuarioRegistrado.experiencia.descripcion);
      this.experienciaForm.controls.tipo_trabajo.setValue(this.usuarioRegistrado.experiencia.tipo_trabajo);
      this.experienciaForm.controls.salario_hora.setValue(this.usuarioRegistrado.experiencia.salario_hora);
     
    
     }
  }

  submitFormExperiencia(){
    if(this.experienciaForm.valid){
      this.usuarioRegistrado.experiencia = this.experienciaForm.value;
    
    
    this._usuarioService.updateUser(this.usuarioRegistrado).subscribe(
      response=>{
        if(!response.user){
          this.status = 'error';
        }else{
          this.status = 'success';
          localStorage.setItem('usuarioData',JSON.stringify(this.usuarioRegistrado));
        }
      },
      error=>{
        let errorMensage = <any>error;
        console.log(errorMensage);
        if(errorMensage != null) this.status= 'error';

      });
    }
  }

}
