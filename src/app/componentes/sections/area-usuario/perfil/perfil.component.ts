import { Component, OnInit } from '@angular/core';
import { UsurioService } from '../../../../services/usurio.service';
import { Usuario } from '../../../../models/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  tabActive:string;
  usuarioRegistrado :Usuario;
  constructor(private _usuarioService:UsurioService,private route: Router ) {
    
   }

  ngOnInit() {
    this.tabActive = 'dUsuario';
    this.usuarioRegistrado = this._usuarioService.getUsuario();
    if(this.usuarioRegistrado.perfil === undefined || this.usuarioRegistrado.perfil.nombre === undefined) this.route.navigate(['/perfil/update']);
  }
  cambiarTab(tabName){
    this.tabActive = tabName;
  }

  isSeccionUsuario(){
    return this.tabActive === 'dUsuario';
  }

  isSeccionCliente(){
    return this.tabActive === 'dIdentify';
    
  }

  isSeccionExperiencia(){
    return this.tabActive === 'dExperiencia';
  }

  isCliente(){
    return this._usuarioService.getRol() === "Cliente";
  }

  isAutonomo(){
    return this._usuarioService.getRol() === "Autonomo";
  }
  
  ngDoCheck(){
    this.usuarioRegistrado = this._usuarioService.getUsuario(); 
  }

}
