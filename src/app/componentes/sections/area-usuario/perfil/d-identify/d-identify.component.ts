import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Usuario } from './../../../../../models/usuario';
import { UsurioService } from './../../../../../services/usurio.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dIdentify',
  templateUrl: './d-identify.component.html',
  styleUrls: ['./d-identify.component.css']
})
export class DIdentifyComponent implements OnInit {
  usuarioRegistrado :Usuario;
  
  constructor(private _usuarioServicie:UsurioService) { }

  ngOnInit() {
    this.usuarioRegistrado = this._usuarioServicie.getUsuario();   
   
  }

  ngDoCheck(){
    this.usuarioRegistrado = this._usuarioServicie.getUsuario();
   
  }

}
