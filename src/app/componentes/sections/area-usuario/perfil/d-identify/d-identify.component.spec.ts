import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DIdentifyComponent } from './d-identify.component';

describe('DIdentifyComponent', () => {
  let component: DIdentifyComponent;
  let fixture: ComponentFixture<DIdentifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DIdentifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DIdentifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
