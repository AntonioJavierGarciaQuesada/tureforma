import { UsurioService } from './../../../../services/usurio.service';
import { ProyectosService } from './../../../../services/proyectos.service';
import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTableDataSource,MatPaginator,MatSort } from '@angular/material';
import { Usuario } from '../../../../models/usuario';


@Component({
  selector: 'app-tus-proyectos',
  templateUrl: './tus-proyectos.component.html',
  styleUrls: ['./tus-proyectos.component.css']
})
export class TusProyectosComponent implements OnInit {
  proyectosList = [];
  proyectoSelect;
  displayedColumns = ['id'];
  dataSource;
  visible: Boolean; 
  usuarioRegistrado:Usuario;
  token:String;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private _proyectoService:ProyectosService,
    private _usuarioService:UsurioService
  ) { }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim(); //quitamos los espacios al filtro
    filterValue = filterValue.toLowerCase(); //cambiamos a minuscula lo introducido en el filtro
    this.dataSource.filter = filterValue;
  }
 

  ngOnInit() {
    this.token = this._usuarioService.getToken();
   
    this._proyectoService.getMisProyectos(this.token)
      .subscribe(
        response=>{
          console.log(response);
          if(response.proyectos){
            this.proyectosList = response.proyectos;
            this.dataSource = new MatTableDataSource(this.proyectosList);
            this.dataSource.paginator = this.paginator;
            this.visible = false;  
          }         
         
        },
        err =>{
          console.log(err);
        }
        
      ) 
      this.usuarioRegistrado = this._usuarioService.getUsuario();
  }

  isCliente(){
    return this.usuarioRegistrado &&  this.usuarioRegistrado.tipo && this.usuarioRegistrado.tipo === 'Cliente';
  }

}
