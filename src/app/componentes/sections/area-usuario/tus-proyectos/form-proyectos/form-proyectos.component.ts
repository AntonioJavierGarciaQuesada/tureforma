import { Usuario } from './../../../../../models/usuario';
import { UsurioService } from './../../../../../services/usurio.service';
import { LocalidadesService } from './../../../../../services/localidades.service';
import { localidades } from './../../../../../mocks/localidades.mocks';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ProyectosService } from '../../../../../services/proyectos.service';

@Component({
  selector: 'app-form-proyectos',
  templateUrl: './form-proyectos.component.html',
  styleUrls: ['./form-proyectos.component.css']
})
export class FormProyectosComponent implements OnInit {
  proyectoForm:FormGroup;
  public comunidades:string[];
  public provincias:string[];
  public localidades:string[];

  public token:String;
  public status:String;
  constructor(
    private _proyectoService:ProyectosService,
    private _localidadesService:LocalidadesService,
    private _usuarioService:UsurioService
  ) { }

  ngOnInit() {
   
    this.token = this._usuarioService.getToken();
    this.proyectoForm = new FormGroup({
      categoria: new FormControl(null,Validators.required),
      size: new FormControl(null,Validators.required),
      descripcion_breve: new FormControl(null,Validators.required),
      descripcion_completa: new FormControl(null,Validators.required),
      duracion: new FormControl(null,Validators.required),
      direccion: new FormControl(null,Validators.required),
      localidad: new FormControl(null,Validators.required),
      provincia: new FormControl(null,Validators.required),
      comunidad: new FormControl(null,Validators.required),
      incluir_material: new FormControl(null,Validators.required),
      precio: new FormControl(null,[
        Validators.required,
      Validators.min(0)])
    }); 
    this.comunidades = this._localidadesService.getComunidades();
       
  }
  selectComunidad(){
    this.provincias  = this._localidadesService.getProvincias(this.proyectoForm.value.comunidad);
  }

  selectProvincia(){    
    this.localidades = this._localidadesService.getLocalidades(this.proyectoForm.value.provincia);   

  }
  submitFormProyecto(){
    if(this.proyectoForm.valid){  
    let proyecto = this.proyectoForm.value;
    this._proyectoService.saveProyecto(proyecto,this.token).subscribe(
      response=>{
        if(!response.proyecto){
          this.status = 'error';
        }else{
          this.status = 'success'; 
          this.proyectoForm.reset();         
        }
      },
      error=>{
        let errorMensage = <any>error;
        console.log(errorMensage);
        if(errorMensage != null) this.status= 'error';

      });
    }
  }
}
