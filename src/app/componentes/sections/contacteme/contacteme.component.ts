import { ContactService } from './../../../services/contact.service';
import { MensajeContacto } from './../../../models/mensajeContacto';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contacteme',
  templateUrl: './contacteme.component.html',
  styleUrls: ['./contacteme.component.css']
})
export class ContactemeComponent implements OnInit {
  contacteForm:FormGroup;
  mensaje_envio:String;
  constructor(private _contactService: ContactService) { 
   
  }

  ngOnInit() {
    this.contacteForm = new FormGroup({
      nombreC: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/),
        Validators.min(3)
      ]),
      apellidosC: new FormControl(null, [
        Validators.required,
        Validators.pattern(/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/),
        Validators.min(3)
      ]),
      telefono: new FormControl(null,[
        Validators.pattern(/^([6-9]{1}[0-9]{8})+$/),
        Validators.min(9)
      ]),
      emailC: new FormControl(null, [
        Validators.required,
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'),
        Validators.min(8)
      ]),
      texto: new FormControl(null, [
        Validators.required
      ])
    });
  }
  onSubmit(){
    if( this.contacteForm.valid ){
      const {nombreC,apellidosC,telefono,emailC,texto} = this.contacteForm.value;
      const mensaje = new MensajeContacto(nombreC,apellidosC,telefono,emailC,texto);
      this.contacteForm.reset();
      this._contactService.enviarMensajeContacto(mensaje)
        .subscribe(
          response => {
            if(response.mensaje)
             this.mensaje_envio=response.mensaje;
          },
          error => {
            console.log(<any>error);
          }
        );

    }
    
  }

}
