import { Usuario } from './../../../../models/usuario';
import { UsurioService } from './../../../../services/usurio.service';
import { LocalidadesService } from './../../../../services/localidades.service';
import { FiltroAutonomo } from './../../../../models/filtroAutonomo';
import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent implements OnInit {
  @Output() filtrar = new EventEmitter();
  usuarioRegistrado:Usuario;
  categoria:String;
  provincia:String;  
  precio_max:Number;
  precio_min:Number;
  provincias:String[];
  constructor(private _localidadesSevice:LocalidadesService,
              private _usuarioService:UsurioService) { }

  ngOnInit() {
    this.usuarioRegistrado = this._usuarioService.getUsuario();
    this.provincias = this._localidadesSevice.getProvincia();
  }

  enviarFiltro(event){
    const filtros : FiltroAutonomo ={
      categoria:this.categoria,
      provincia: this.provincia,
      precio_max: this.precio_max,
      precio_min: this.precio_min
    }    
    
    this.filtrar.emit(filtros);
  }

  resetFiltro(event){
    const filtros : FiltroAutonomo ={
      categoria:undefined,
      provincia: undefined,
      precio_max: undefined,
      precio_min: undefined
    }    
    this.categoria= undefined;
    this.precio_min = undefined;
    this.precio_max = undefined;
    this.provincia = undefined;
    this.filtrar.emit(filtros);
  
  }

  isCliente(){
    return this.usuarioRegistrado && this.usuarioRegistrado.tipo==="Cliente";
  }
}
