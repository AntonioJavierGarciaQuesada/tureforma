import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutonomosComponent } from './autonomos.component';

describe('AutonomosComponent', () => {
  let component: AutonomosComponent;
  let fixture: ComponentFixture<AutonomosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutonomosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutonomosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
