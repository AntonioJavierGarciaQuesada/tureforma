import { FiltroAutonomo } from './../../../models/filtroAutonomo';
import { GLOBAL } from './../../../services/global';
import { Component, OnInit, Input } from '@angular/core';
import { UsurioService } from '../../../services/usurio.service';
import { Usuario } from '../../../models/usuario';

@Component({
  selector: 'app-autonomos',
  templateUrl: './autonomos.component.html',
  styleUrls: ['./autonomos.component.css']
})
export class AutonomosComponent implements OnInit {
  autonomosList:Usuario[];
 
  usuarioRegistrado:Usuario;
  url:String;
   filtros: FiltroAutonomo = {categoria:"",provincia:"",precio_min:0,precio_max:0};
  constructor(private _usuarioService:UsurioService) { }

  ngOnInit() {
    
    this.usuarioRegistrado = this._usuarioService.getUsuario();
    this._usuarioService.getAutonomos()
    .subscribe(
      response =>{
        console.log(response);
        if(response.status === "success"){
          this.autonomosList = response.autonomos.filter(autonomo=>autonomo.experiencia && autonomo.experiencia.tipo_habilidad);
         
        }else  
          this.autonomosList = [];  
      },
      err=>{
        console.log(err);
      }
    );
    this.url = GLOBAL.url;
  }

  ngCheckDo(){
    
  }
  isCliente(){
    return this.usuarioRegistrado &&  this.usuarioRegistrado.tipo && this.usuarioRegistrado.tipo === 'Cliente';
  }
  recibirFiltros(event){    
    this.filtros = event;  
  }
  
    

}
