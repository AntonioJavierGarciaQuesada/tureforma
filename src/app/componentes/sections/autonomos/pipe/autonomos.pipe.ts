import { FiltroAutonomo } from './../../../../models/filtroAutonomo';
import { Usuario } from './../../../../models/usuario';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autonomosFiltro',
  pure: true
})
export class AutonomosPipe implements PipeTransform {

  transform(autonomosList: Usuario[] , filtros: FiltroAutonomo): Usuario[] {
    let autonomosFiltrado = autonomosList;
    if( filtros && filtros.categoria){
     autonomosFiltrado = autonomosFiltrado.filter(autonomo => autonomo.experiencia.tipo_habilidad == filtros.categoria ); 
    }
    if(filtros && filtros.provincia){
      autonomosFiltrado = autonomosFiltrado.filter(autonomo=> autonomo.perfil.provincia == filtros.provincia)
    }
    if(filtros && filtros.precio_min){
      autonomosFiltrado = autonomosFiltrado.filter(autonomo=> autonomo.experiencia.salario_hora>=filtros.precio_min);
    }
    if(filtros && filtros.precio_max){
      autonomosFiltrado = autonomosFiltrado.filter(autonomo=> autonomo.experiencia.salario_hora<=filtros.precio_max);
    }
    return autonomosFiltrado;  
  
  }

}
