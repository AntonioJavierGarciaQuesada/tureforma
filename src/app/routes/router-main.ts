import { RecuperarComponent } from './../componentes/navbar/recuperar/recuperar.component';
import { LoginComponent } from './../componentes/navbar/login/login.component';
import { FormProyectosComponent } from './../componentes/sections/area-usuario/tus-proyectos/form-proyectos/form-proyectos.component';
import { AutonomosComponent } from './../componentes/sections/autonomos/autonomos.component';
import { FormPerfilComponent } from './../componentes/sections/area-usuario/perfil/form-perfil/form-perfil.component';
import { PerfilComponent } from './../componentes/sections/area-usuario/perfil/perfil.component';
//import Componentes
import { ComoFuncionaComponent } from './../componentes/sections/como-funciona/como-funciona.component';
import { ContactemeComponent } from './../componentes/sections/contacteme/contacteme.component';
import { MainComponent } from './../componentes/sections/main/main.component';

//import de angular
import { Routes,RouterModule} from '@angular/router';
import {ModuleWithProviders, ModuleWithComponentFactories } from '@angular/core';
import { FormExperienciaComponent } from '../componentes/sections/area-usuario/perfil/form-experiencia/form-experiencia.component';
import { ProyectosComponent } from '../componentes/sections/proyectos/proyectos.component';
import { TusProyectosComponent } from '../componentes/sections/area-usuario/tus-proyectos/tus-proyectos.component';
import { RegistroComponent } from '../componentes/navbar/registro/registro.component';


const  routesMain: Routes = [
    {path: '',redirectTo: '/',pathMatch: 'full'},
    {path: '',component: MainComponent },
    {path:'login',component:LoginComponent},
    {path: 'registro',component:RegistroComponent},    
    {path: 'proyectos',component:ProyectosComponent},
    {path: 'autonomos',component: AutonomosComponent},
    {path: 'contacto',component: ContactemeComponent},
    {path: 'como_funciona',component: ComoFuncionaComponent},
    {path: 'perfil',component: PerfilComponent},
    {path: 'perfil/update',component: FormPerfilComponent },
    {path: 'perfil/update-experiencia',component: FormExperienciaComponent },
    {path: 'perfil/proyectos',component: TusProyectosComponent},
    {path: 'perfil/proyectos/crear',component: FormProyectosComponent}
];   

export const routes: ModuleWithProviders = RouterModule.forRoot(routesMain);