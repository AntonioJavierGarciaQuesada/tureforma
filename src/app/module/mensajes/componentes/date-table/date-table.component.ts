import { Mensaje } from './../../../../models/mensaje';
import { Component, OnInit,ViewChild, Input } from '@angular/core';
import { MatTableDataSource,MatPaginator,MatSort } from '@angular/material';

@Component({
  selector: 'app-date-table',
  templateUrl: './date-table.component.html',
  styleUrls: ['./date-table.component.css']
})
export class DateTableComponent implements OnInit {
  displayedColumns = ['receptor.email','texto','fecha_creacion','consultar'];
  mensajes;
  dataSource = new MatTableDataSource(this.mensajes.filter(mensaje=> {
    return {email:mensaje.receptor.email,texto:mensaje.texto,fechac_creacion:mensaje.fecha_creacion};
  }));
  mensajeSelect: Mensaje;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  visible:Boolean;

  applyFilter(filterValue: string){
    
    filterValue = filterValue.trim(); //quitamos los espacios al filtro
    filterValue = filterValue.toLowerCase(); //cambiamos a minuscula lo introducido en el filtro
    this.dataSource.filter = filterValue;
  }
  constructor() {
    console.log(this.mensajes);
   }

  ngOnInit() {
    
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.visible = false;
  }
  consultar(mensaje){
    mensaje.visualizado = true;
    this.mensajeSelect = mensaje;
    this.visible = true;
   
  }
}
