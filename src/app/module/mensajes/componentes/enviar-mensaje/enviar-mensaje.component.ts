import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsurioService } from './../../../../services/usurio.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from './../../../../services/global';
import { Mensaje } from './../../../../models/mensaje';
import { MensajeService } from './../../../../services/mensaje.service';
import { Usuario } from '../../../../models/usuario';



@Component({
  selector: 'app-enviar-mensaje',
  templateUrl: './enviar-mensaje.component.html',
  styleUrls: ['./enviar-mensaje.component.css'],
  providers: [MensajeService]
})
export class EnviarMensajeComponent implements OnInit {
  public title: string;
  public mensaje: Mensaje;
  public usuarioRegistrado;
  public token:String;
  public url: String;
  public status: String;
  public envioMensajeForm: FormGroup;
  private receptor: Usuario;
  private receptorList:Usuario[];

  constructor(
    private _route: ActivatedRoute,  
    private _mensajeService: MensajeService,
    private _userService: UsurioService,
    private _router:Router
  ) {
    this.usuarioRegistrado = this._userService.getUsuario();
    if(!this.usuarioRegistrado || !this.usuarioRegistrado._id) this._router.navigate(['/']);
    this.title = "Enviar Mensaje";
    if(this.usuarioRegistrado.tipo === 'Cliente'){
      this._userService.getAutonomos()
        .subscribe(
          response=>{
            this.receptorList = response.autonomos;
          },
          err =>{
            console.log('Error'+ err);
          }
        );  
      }else{
        this._userService.getClientes()
        .subscribe(
          response=>{
            this.receptorList = response.clientes;
          },
          err =>{
            console.log('Error'+ err);
          }
        );  
      }        
    
    
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.mensaje = new Mensaje(null,null,null,"",null,false);
    this._route.params
      .subscribe(params => 
        {this._userService.getUsuarioById(params['id'])
          .subscribe(response=>{
            this.mensaje.receptor =response.usuario._id;
            this.envioMensajeForm.controls.receptor.setValue(response.usuario._id);
          }) 
    });
   }

  ngOnInit() {

    
    this.envioMensajeForm = new FormGroup({
      receptor: new FormControl(null,Validators.required),
      texto: new FormControl(null,Validators.required)
    });
    
    console.log('component.enviar-mensaje cargado...');
  }

  onSubmit(){
    if(this.envioMensajeForm.valid){
      this.mensaje.texto = this.envioMensajeForm.value.texto;
      this.mensaje.receptor = this.envioMensajeForm.value.receptor;
      console.log(this.token);
      console.log(this.mensaje);
      this._mensajeService.enviarMensaje(this.token,this.mensaje)
        .subscribe(
          response => {
            if(response.mensaje){
              this.status="success";
              this.envioMensajeForm.reset();
            }

          },
          err => {
            this.status = 'error';
            console.log(<any>err);
          }
        )

    }
  }

  redirect(ruta){
    this._router.navigate([ruta]);
  }
}
