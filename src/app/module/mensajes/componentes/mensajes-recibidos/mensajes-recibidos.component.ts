import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsurioService } from './../../../../services/usurio.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit,ViewChild, Input } from '@angular/core';
import { MatTableDataSource,MatPaginator,MatSort } from '@angular/material';
import { GLOBAL } from './../../../../services/global';
import { Mensaje } from './../../../../models/mensaje';
import { MensajeService } from './../../../../services/mensaje.service';

@Component({
  selector: 'app-mensajes-recibidos',
  templateUrl: './mensajes-recibidos.component.html',
  styleUrls: ['./mensajes-recibidos.component.css']
})
export class MensajesRecibidosComponent implements OnInit {
 
  public title: String;
  public mensajeList=[];

  public usuarioRegistrado;
  public token:String;
  public url: String;
  public status: String;
  //DataTable
  displayedColumns = ['emailReceptor','texto','createdAt','consultar'];
  dataSource;
  mensajeSelect;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  visible:Boolean;


  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _mensajeService: MensajeService,
    private _userService: UsurioService
  ) {
    this.title = "Mensajes recibidos";
  }

  ngOnInit() {
    this.token = this._userService.getToken();  
    this._mensajeService.getMensajesRecibidos(this.token,1)
      .subscribe(
        response=> {          
          if(response.mensajes){
            this.mensajeList = response.mensajes;  
            this.dataSource = new MatTableDataSource(this.mensajeList.filter(mensaje=> {
                return {emailReceptor:mensaje.receptor.email,texto:mensaje.texto,fecha_creacion:mensaje.fecha_creacion};
            }));
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.visible = false;  
          }
        
    
          
        },
        err=>{
          console.log(<any>err);
        }
      ); 

    console.log('component.mensaje-recibidos cargado...');
     
  }

  

  ngDoCheck(){
    this._mensajeService.getMensajesRecibidos(this.token,1)
      .subscribe(
        response=> {          
          if(response.mensajes){
            this.mensajeList = response.mensajes;  
            this.dataSource = new MatTableDataSource(this.mensajeList.filter(mensaje=> {
                return {emailReceptor:mensaje.receptor.email,texto:mensaje.texto,fecha_creacion:mensaje.fecha_creacion};
            }));
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            this.visible = false;  
          }
        
    
          
        },
        err=>{
          console.log(<any>err);
        }
      ); 
  }

  applyFilter(filterValue: string){
    
    filterValue = filterValue.trim(); //quitamos los espacios al filtro
    filterValue = filterValue.toLowerCase(); //cambiamos a minuscula lo introducido en el filtro
    this.dataSource.filter = filterValue;
  }
  
  consultar(mensaje){
    mensaje.visualizado = true;
    this.mensajeSelect = mensaje;
    this.visible = true;
  }
  

}
