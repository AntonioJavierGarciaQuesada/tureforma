import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajesRecibidosComponent } from './mensajes-recibidos.component';

describe('MensajesRecibidosComponent', () => {
  let component: MensajesRecibidosComponent;
  let fixture: ComponentFixture<MensajesRecibidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajesRecibidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajesRecibidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
