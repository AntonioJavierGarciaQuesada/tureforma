import { MomentModule } from 'angular2-moment';
//Modules
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Rutas
import { MensajeRoutingModule } from './mensaje-routing.component';

//Componentes
import { MensajesRecibidosComponent } from './componentes/mensajes-recibidos/mensajes-recibidos.component';
import { MensajesEnviadosComponent } from './componentes/mensajes-enviados/mensajes-enviados.component';
import { EnviarMensajeComponent } from './componentes/enviar-mensaje/enviar-mensaje.component';
import { MainComponent } from './componentes/main/main.component';
import { DateTableComponent } from './componentes/date-table/date-table.component';
import { MaterialModule } from '../material.module';

@NgModule({
    declarations: [
        MainComponent,
        EnviarMensajeComponent,
        MensajesEnviadosComponent,
        MensajesRecibidosComponent,
        DateTableComponent
        
    ],
    imports: [
        CommonModule,
        FormsModule,
        MensajeRoutingModule,
        ReactiveFormsModule,
        MaterialModule,
        MomentModule
    ],
    exports: [
        MainComponent,
        EnviarMensajeComponent,
        MensajesEnviadosComponent,
        MensajesRecibidosComponent
    ],
    providers: []
})
export class MensajesModule {}