//Modulos
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
//componentes
import { MensajesRecibidosComponent } from './componentes/mensajes-recibidos/mensajes-recibidos.component';
import { MensajesEnviadosComponent } from './componentes/mensajes-enviados/mensajes-enviados.component';
import { EnviarMensajeComponent } from './componentes/enviar-mensaje/enviar-mensaje.component';
import { MainComponent } from './componentes/main/main.component';

const mensajesRoutes: Routes = [
    {
        path: 'mensajes', 
        component: MainComponent,
        children: [
            { path:'',redirectTo:'recibidos',pathMatch: 'full'},
            { path:'enviar',component:EnviarMensajeComponent},
            { path:'enviar/:id',component:EnviarMensajeComponent},
            { path:'recibidos',component:MensajesRecibidosComponent},
            { path:'enviados',component:MensajesEnviadosComponent}
        ]    
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(mensajesRoutes)
    ],
    exports:[
        RouterModule
    ]
})
export class MensajeRoutingModule {}

