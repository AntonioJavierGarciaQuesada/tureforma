import { NgModule } from '@angular/core';

import {MatButtonModule, MatCheckboxModule, MatIconModule, MatMenuModule, MatTooltipModule, MatStepperModule, MatDatepickerModule, MatInputModule, MatSelectModule, MatNativeDateModule, MatTableModule, MatSortModule, MatPaginatorModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material/toolbar';
import{ MatSidenavModule} from '@angular/material/sidenav'

//Vector de modules a exportar
const modules = [
    MatButtonModule, 
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule ,
    MatTooltipModule,
    MatStepperModule,
    MatDatepickerModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSidenavModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
   ];

@NgModule({
  imports: modules,
  exports: modules,
  declarations: [],
})
export class MaterialModule { }